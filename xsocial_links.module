<?php

use Drupal\file\Entity\File;
use Drupal\xsocial_links\Entity\SocialLink;

/**
 * Implements hook_theme().
 */
function xsocial_links_theme(): array {
  return [
    'social_links' => [
      'variables' => [
        'links' => [],
      ],
    ],
  ];
}

/**
 * Preprocess function for social-links.html.twig.
 */
function xsocial_links_preprocess_social_links(array &$vars): void {
  if (!empty($vars['links'])) {
    $social_links = $vars['links']; /** @var SocialLink[] $social_links */
    foreach ($social_links as $key => $social_link) {
      $vars['links'][$key] = [
        'entity' => $social_link,
        'id' => $social_link->id(),
        'label' => $social_link->label(),
        'url' => $social_link->get('url'),
        'icon' => [],
      ];

      if ($social_link_fids = $social_link->get('fid')) {
        $files = File::loadMultiple($social_link_fids);
        foreach ($files as $file) {
          $vars['links'][$key]['icon'][] = $file->getFileUri();
        }
      }
    }
  }
}

/**
 * Implements hook_filter_functions().
 */
function xsocial_links_filter_functions(): array {
  return [
    'social_links' => [
      'function' => 'function_filter__social_links',
    ],
  ];
}

/**
 * Return social links.
 */
function function_filter__social_links(): array {
  return [
    '#theme' => 'social_links',
    '#links' => \Drupal::entityTypeManager()->getStorage('social_link')->loadByProperties(['status' => 1]),
  ];
}
