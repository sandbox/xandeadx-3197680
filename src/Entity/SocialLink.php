<?php

namespace Drupal\xsocial_links\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * @ConfigEntityType(
 *   id = "social_link",
 *   label = @Translation("Social link"),
 *   handlers = {
 *     "list_builder" = "\Drupal\improvements\DefaultDraggableEntityListBuilder",
 *     "form" = {
 *       "add" = "\Drupal\xsocial_links\Form\SocialLinkForm",
 *       "edit" = "\Drupal\xsocial_links\Form\SocialLinkForm",
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *     }
 *   },
 *   config_prefix = "social_link",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "url",
 *     "fid",
 *     "weight",
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/social_links",
 *     "edit-form" = "/admin/config/system/social_links/{social_link}",
 *     "delete-form" = "/admin/config/system/social_links/{social_link}/delete",
 *   }
 * )
 */
class SocialLink extends ConfigEntityBase {

  protected $id;

  protected $label;

  protected $url;

  protected $fid;

  protected $weight;

}
