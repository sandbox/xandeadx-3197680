<?php

namespace Drupal\xsocial_links\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\xsocial_links\Entity\SocialLink;

#[Block(
  id: 'social_links_block',
  admin_label: new TranslatableMarkup('Social links'),
)]
class SocialLinksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];

    $social_links_ids = \Drupal::entityQuery('social_link')
      ->condition('status', 1)
      ->condition('url', '', '<>')
      ->sort('weight')
      ->execute();

    if ($social_links_ids) {
      $build['content'] = [
        '#theme' => 'social_links',
        '#links' => SocialLink::loadMultiple($social_links_ids),
      ];

      $build['#contextual_links']['social_links'] = [
        'route_parameters' => [],
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['config:social_link_list']);
  }

}
