<?php

namespace Drupal\xsocial_links\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\xsocial_links\Entity\SocialLink;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SocialLinkForm extends EntityForm {

  protected FileUsageInterface $fileUsage;

  /**
   * Form conscructor.
   */
  public function __construct(FileUsageInterface $file_usage) {
    $this->fileUsage = $file_usage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('file.usage'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $social_link = $this->entity; /** @var SocialLink $social_link */

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID'),
      '#default_value' => $social_link->id(),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $social_link->label(),
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $social_link->get('url'),
    ];

    $form['icon'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Icon'),
      '#upload_location' => 'public://social-icons',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg png gif webp svg'],
      ],
      '#default_value' => $social_link->get('fid'),
      '#multiple' => TRUE,
    ];

    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#default_value' => $social_link->get('weight'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $social_link->status(),
    ];

    // Disable cache to avoid errors with storing files in tempstore
    $form_state->disableCache();

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $old_fids = $this->entity->get('fid') ?: [];
    $current_fids = $form_state->getValue('icon');
    $new_fids = array_diff($current_fids, $old_fids);
    $removed_fids = array_diff($old_fids, $current_fids);

    // Delete removed files
    foreach ($removed_fids as $fid) {
      if ($file = File::load($fid)) {
        $file->delete();
      }
    }

    // Save new files
    foreach ($new_fids as $fid) {
      if ($file = File::load($fid)) {
        // Set permanent status
        if ($file->isTemporary()) {
          $file->setPermanent();
          $file->save();
        }

        // Add row in file_usage table
        $this->fileUsage->add($file, 'social_links', 'social_link', $form_state->getValue('id'));
      }
    }

    // Massage values
    $form_state->setValue('fid', $current_fids);

    parent::submitForm($form, $form_state);

    $this->messenger()->addStatus($this->t('Social link saved.'));
    $form_state->setRedirect('entity.social_link.collection');
  }

}
